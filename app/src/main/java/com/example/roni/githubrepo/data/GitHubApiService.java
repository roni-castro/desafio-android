package com.example.roni.githubrepo.data;

import com.example.roni.githubrepo.model.PullRequestModel;
import com.example.roni.githubrepo.model.RepositoryResponse;
import com.example.roni.githubrepo.model.UserModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by roni on 18/02/18.
 */

public interface GitHubApiService {
    @GET("search/repositories")
    Single<RepositoryResponse> getRepositoryList(@Query("q") String q,
                                                 @Query("sort") String sort,
                                                 @Query("page") int page);

    @GET("repos/{creator_of_repository}/{repository_name}/pulls")
    Single<List<PullRequestModel>> getPullRequestList(
            @Path("creator_of_repository") String creatorOfRepository,
            @Path("repository_name") String repositoryName,
            @Query("page") int page);
}
