package com.example.roni.githubrepo.di.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by roni on 21/02/18.
 */
@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context){
        this.context = context;
    }

    @Provides
    public Context context(){
        return context.getApplicationContext();
    }
}
