package com.example.roni.githubrepo.ui.repo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.roni.githubrepo.R;
import com.example.roni.githubrepo.di.component.DaggerGitHubRepoComponent;
import com.example.roni.githubrepo.di.component.GitHubRepoComponent;
import com.example.roni.githubrepo.di.module.AppModule;
import com.example.roni.githubrepo.di.module.ContextModule;
import com.example.roni.githubrepo.model.RepositoryModel;
import com.example.roni.githubrepo.ui.base.BaseFragment;
import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailActivity;
import com.example.roni.githubrepo.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by roni on 18/02/18.
 */

public class GitHubRepoFragment extends BaseFragment implements GitHubRepoContract.AppView, GitHubRepoAdapterCellClickListener {

    @BindView(R.id.recycler_view_github_repo) RecyclerView mRecyclerView;
    GitHubRepoAdapter mAdapter;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    // Variables used to save the state tof the repo list during rotation
    private static final String LAYOUT_MANAGER_POSITION_STATE_KEY = "LAYOUT_MANAGER_POSITION_STATE_KEY";
    private static final String LIST_STATE_KEY = "LIST_REPO_STATE_KEY";
    private static final String CURRENT_PAGE_STATE_KEY = "CURRENT_PAGE_STATE_KEY";

    @Inject
    GitHubRepoContract.Presenter<GitHubRepoContract.AppView> presenter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of this fragment .
     */
    public static GitHubRepoFragment newInstance() {
        return new GitHubRepoFragment();
    }

    // LIFECYCLE METHODS
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if(savedInstanceState == null){
           setUpRecyclerViewWithNoData();
           presenter.getGitHubRepoAtPage(endlessRecyclerOnScrollListener.getCurrentPage());
        } else{ // Recover list state
            setUpRecyclerViewWithExistingData(savedInstanceState);
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save list state
        outState.putParcelableArrayList(LIST_STATE_KEY, mAdapter.getItems());
        outState.putInt(LAYOUT_MANAGER_POSITION_STATE_KEY, mRecyclerView.getVerticalScrollbarPosition());
        outState.putInt(CURRENT_PAGE_STATE_KEY, endlessRecyclerOnScrollListener.getCurrentPage());
    }

    // ABSTRACT METHODS IMPLEMENTATION
    @Override
    protected int getFragmentViewResId() {
        return R.layout.fragment_github_repo;
    }

    @Override
    protected void setupInjectionComponent() {
        GitHubRepoComponent gitHubRepoComponent = DaggerGitHubRepoComponent.builder()
                .contextModule(new ContextModule(getActivity()))
                .appModule(new AppModule())
                .build();
        gitHubRepoComponent.inject(this);
    }

    @Override
    public void attachViewToPresenter() {
        presenter.onAttachView(this);
    }

    @Override
    public void detachViewToPresenter() {
        presenter.onDetachView();
    }

    // HELPER METHODS
    private void setUpRecyclerView(LinearLayoutManager linearLayoutManager,
                                   GitHubRepoAdapter adapter,
                                   int currentPage){
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setAdapter(adapter);
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                presenter.getGitHubRepoAtPage(page);
            }
        };
        endlessRecyclerOnScrollListener.setCurrentPage(currentPage);
        mRecyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);
    }

    private void setUpRecyclerViewWithNoData(){
        mAdapter = new GitHubRepoAdapter(getActivity(),this, new ArrayList<>());
        setUpRecyclerView(new LinearLayoutManager(getActivity()), mAdapter, 1);
    }

    private void setUpRecyclerViewWithExistingData(Bundle savedInstanceState){
        ArrayList<RepositoryModel> repositoryModels = savedInstanceState.getParcelableArrayList(LIST_STATE_KEY);
        int verticalScrollPosition = savedInstanceState.getInt(LAYOUT_MANAGER_POSITION_STATE_KEY);
        int currentPageState = savedInstanceState.getInt(CURRENT_PAGE_STATE_KEY);

        mRecyclerView.setVerticalScrollbarPosition(verticalScrollPosition);
        mAdapter = new GitHubRepoAdapter(getActivity(),this, repositoryModels);
        setUpRecyclerView(new LinearLayoutManager(getActivity()), mAdapter, currentPageState);
    }

    @Override
    public void onRepositoryCellClick(RepositoryModel repositoryModel) {
        presenter.onRepoCellClick(repositoryModel);
    }

    @Override
    public void goToGitHubDetailActivity(RepositoryModel repositoryModel) {
        Intent intent = new Intent(getActivity(), GitHubRepoDetailActivity.class);
        intent.putExtra(GitHubRepoActivity.REPOSITORY_NAME_EXTRA, repositoryModel.getName());
        intent.putExtra(GitHubRepoActivity.OWNER_LOGIN_EXTRA, repositoryModel.getUserModel().getUserName());
        startActivity(intent);
    }

    @Override
    public void addReposItemToList(List<RepositoryModel> repositoryModelList) {
        mAdapter.addAllItemsAtEndOfList(repositoryModelList);
    }
}
