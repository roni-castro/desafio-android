package com.example.roni.githubrepo.ui.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.roni.githubrepo.GitHubApplication;
import com.example.roni.githubrepo.utils.ProgressBarHandler;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by roni on 17/02/18.
 */

public abstract class BaseFragment extends Fragment implements BaseView {
    private Unbinder mUnbinder;
    private ProgressBarHandler mProgressBarHandler;

    // METHODS TO BE IMPLEMENTED BY ALL FRAGMENT VIEWS
    // Fragment layout id
    protected abstract @LayoutRes int getFragmentViewResId();
    // Inject this view to the activity component
    protected abstract void setupInjectionComponent();
    // Associate the child fragment view to the View object in the Presenter
    public abstract void attachViewToPresenter();
    // Detach the child fragment view to the View object in the Presenter
    public abstract void detachViewToPresenter();

    // LIFE CYCLE METHODS
    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupInjectionComponent();
    }

    @Nullable
    @CallSuper
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getFragmentViewResId(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        attachViewToPresenter();
        return view;
    }

    @CallSuper
    @Override
    public void onDestroy() {
        if(mUnbinder != null){
            mUnbinder.unbind();
        }
        detachViewToPresenter();
        super.onDestroy();
    }

    // BASE VIEW METHODS
    @Override
    public void showMessage(int stringResId) {
        showMessage(getString(stringResId));
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(GitHubApplication.getInstance().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoading() {
        if(mProgressBarHandler == null) {
            mProgressBarHandler = new ProgressBarHandler(getActivity());
        }
        mProgressBarHandler.show();
    }

    @Override
    public void hideLoading() {
        mProgressBarHandler.hide();
    }
}
