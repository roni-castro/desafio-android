package com.example.roni.githubrepo.ui.base;

import android.support.annotation.CallSuper;
import android.util.Log;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by roni on 18/02/18.
 */

public abstract class BaseRxPresenter<V extends BaseView> implements BasePresenterContract<V> {
    protected V appView;

    @Inject
    protected CompositeDisposable subscriptions;

    public BaseRxPresenter(CompositeDisposable compositeDisposable){
        this.subscriptions = compositeDisposable;
    }

    @CallSuper
    @Override
    public void onAttachView(V view) {
        this.appView = view;
    }


    @CallSuper
    @Override
    public void onDetachView() {
        subscriptions.clear();
        appView = null;
    }

    public V getView(){
        if(appView == null) {
            Log.e("BASE PRESENTER", "It's necessary to attach the view to the Presenter");
        }
        return appView;
    }

    public CompositeDisposable getCompositeDisposable() {
        return subscriptions;
    }
}

