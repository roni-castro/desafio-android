package com.example.roni.githubrepo.di.module;

import com.example.roni.githubrepo.ui.repo.GitHubRepoContract;
import com.example.roni.githubrepo.ui.repo.GitHubRepoPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by roni on 20/02/18.
 */

@Module
public class GitHubRepoModule {

    @Provides
    GitHubRepoContract.Presenter<GitHubRepoContract.AppView> provideGitHubRepoPresenter(
            GitHubRepoPresenter<GitHubRepoContract.AppView> presenter) {
        return presenter;
    }
}
