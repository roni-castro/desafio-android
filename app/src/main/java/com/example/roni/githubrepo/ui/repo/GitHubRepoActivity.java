package com.example.roni.githubrepo.ui.repo;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.roni.githubrepo.R;
import com.example.roni.githubrepo.ui.base.BaseActivity;

public class GitHubRepoActivity extends BaseActivity {
    private static final String GITHUB_REPO_FRAGMENT = "GITHUB_REPO_FRAGMENT";
    public static final String OWNER_LOGIN_EXTRA = "OWNER_LOGIN_EXTRA";
    public static final String REPOSITORY_NAME_EXTRA = "REPOSITORY_NAME_EXTRA";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpToolbar();
        // Set up the fragment of this activity
        GitHubRepoFragment fragment = (GitHubRepoFragment) getSupportFragmentManager().findFragmentByTag(GITHUB_REPO_FRAGMENT);
        if(fragment == null){
            fragment = GitHubRepoFragment.newInstance();
        }
        setFragment(R.id.fragment_holder, fragment, GITHUB_REPO_FRAGMENT);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_git_hub_repo;
    }

    private void setUpToolbar(){
        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle(R.string.github_repo_title);
        }
    }
}
