package com.example.roni.githubrepo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by roni on 18/02/18.
 */

public class RepositoryModel implements Parcelable {
    @Expose private long id;
    @Expose private String name;
    @Expose private String description;
    @SerializedName("forks_count") @Expose private int numberOfForks;
    @SerializedName("stargazers_count") @Expose private int numberOfStars;
    @SerializedName("owner") @Expose private UserModel user;

    public RepositoryModel(long id, String name, String description,
                           int numberOfForks, int numberOfStars, UserModel user) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.numberOfForks = numberOfForks;
        this.numberOfStars = numberOfStars;
        this.user = user;
    }

    protected RepositoryModel(Parcel in) {
        id = in.readLong();
        name = in.readString();
        description = in.readString();
        numberOfForks = in.readInt();
        numberOfStars = in.readInt();
    }

    public static final Creator<RepositoryModel> CREATOR = new Creator<RepositoryModel>() {
        @Override
        public RepositoryModel createFromParcel(Parcel in) {
            return new RepositoryModel(in);
        }

        @Override
        public RepositoryModel[] newArray(int size) {
            return new RepositoryModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfForks() {
        return numberOfForks;
    }

    public void setNumberOfForks(int numberOfForks) {
        this.numberOfForks = numberOfForks;
    }

    public int getNumberOfStars() {
        return numberOfStars;
    }

    public void setNumberOfStars(int numberOfStars) {
        this.numberOfStars = numberOfStars;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserModel getUserModel() {
        return user;
    }

    public void setUserModel(UserModel user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeInt(numberOfForks);
        parcel.writeInt(numberOfStars);
    }
}
