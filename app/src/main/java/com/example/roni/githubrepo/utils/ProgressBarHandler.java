package com.example.roni.githubrepo.utils;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

/**
 * Created by roni on 19/02/18.
 */

public class ProgressBarHandler {
    private ProgressBar mProgressBar;

    public ProgressBarHandler(Activity context) {
        // Find the root view of this activity
        ViewGroup rootLayout = (ViewGroup) (context).findViewById(android.R.id.content).getRootView();

        // Create the progress bar
        mProgressBar = new ProgressBar(context, null, android.R.attr.progressBarStyle);
        mProgressBar.setIndeterminate(true);

        // Create Relative layout, which will hold the progressbar, with the constraints
        RelativeLayout.LayoutParams layoutParams = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout rl = new RelativeLayout(context);
        rl.setGravity(Gravity.CENTER);
        rl.addView(mProgressBar);

        // Add the layout holding the progress bar to the root view
        rootLayout.addView(rl, layoutParams);

        // Start the progress bar hidden
        hide();
    }

    public void show() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hide() {
        mProgressBar.setVisibility(View.GONE);
    }
}