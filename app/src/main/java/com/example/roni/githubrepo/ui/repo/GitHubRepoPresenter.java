package com.example.roni.githubrepo.ui.repo;

import android.support.annotation.IdRes;

import com.example.roni.githubrepo.data.GitHubApiService;
import com.example.roni.githubrepo.data.SchedulerProviderContract;
import com.example.roni.githubrepo.model.RepositoryModel;
import com.example.roni.githubrepo.model.RepositoryResponse;
import com.example.roni.githubrepo.ui.base.BaseRxPresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by roni on 18/02/18.
 */

public class GitHubRepoPresenter<V extends GitHubRepoContract.AppView> extends BaseRxPresenter<V>
        implements GitHubRepoContract.Presenter<V> {

    @Inject
    GitHubApiService gitHubApiService;
    @Inject
    SchedulerProviderContract schedulerProviderContract;

    @Inject
    public GitHubRepoPresenter(GitHubApiService gitHubApiService,
                               SchedulerProviderContract schedulerProviderContract,
                               CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
        this.schedulerProviderContract = schedulerProviderContract;
        this.gitHubApiService = gitHubApiService;
    }

    @Override
    public void onRepoCellClick(RepositoryModel repositoryModel) {
        getView().goToGitHubDetailActivity(repositoryModel);
    }

    public void getGitHubRepoAtPage(final int page) {
        getCompositeDisposable().add(
            gitHubApiService.getRepositoryList("language:Java", "stars", page)
                .subscribeOn(schedulerProviderContract.io())
                .observeOn(schedulerProviderContract.ui())
                .doOnSubscribe(__ -> getView().showLoading())
                .doAfterTerminate(() -> getView().hideLoading())
                .subscribeWith(new DisposableSingleObserver<RepositoryResponse>() {

                    @Override
                    public void onSuccess(RepositoryResponse repositoryResponseList) {
                        List<RepositoryModel> repositoryModels = repositoryResponseList.getRepositoryList();
                        getView().addReposItemToList(repositoryModels);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showMessage(e.getLocalizedMessage());
                    }
                })
        );
    }
}
