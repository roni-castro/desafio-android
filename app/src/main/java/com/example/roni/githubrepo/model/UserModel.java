package com.example.roni.githubrepo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by roni on 18/02/18.
 */

public class UserModel {
    @Expose private long id;
    @SerializedName("login") @Expose private String userName;
    @SerializedName("avatar_url") @Expose private String avatarUrl;

    public UserModel(long id, String userName, String avatarUrl) {
        this.id = id;
        this.userName = userName;
        this.avatarUrl = avatarUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
