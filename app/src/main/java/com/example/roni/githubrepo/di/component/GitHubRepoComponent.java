package com.example.roni.githubrepo.di.component;

import com.example.roni.githubrepo.di.module.AppModule;
import com.example.roni.githubrepo.di.module.GitHubRepoModule;
import com.example.roni.githubrepo.di.module.NetworkModule;
import com.example.roni.githubrepo.ui.repo.GitHubRepoFragment;

import dagger.Component;

/**
 * Created by roni on 18/02/18.
 */

@Component(modules = {NetworkModule.class, AppModule.class, GitHubRepoModule.class})
public interface GitHubRepoComponent {

    void inject(GitHubRepoFragment fragment);
}
