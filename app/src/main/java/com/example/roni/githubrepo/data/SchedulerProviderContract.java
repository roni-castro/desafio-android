package com.example.roni.githubrepo.data;

import io.reactivex.Scheduler;

/**
 * Created by roni on 18/02/18.
 */

public interface SchedulerProviderContract {
    Scheduler computation();
    Scheduler io();
    Scheduler ui();
}
