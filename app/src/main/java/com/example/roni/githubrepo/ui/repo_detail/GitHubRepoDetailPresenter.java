package com.example.roni.githubrepo.ui.repo_detail;

import com.example.roni.githubrepo.data.GitHubApiService;
import com.example.roni.githubrepo.data.SchedulerProviderContract;
import com.example.roni.githubrepo.model.PullRequestModel;
import com.example.roni.githubrepo.ui.base.BaseRxPresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by roni on 20/02/18.
 */

public class GitHubRepoDetailPresenter<V extends GitHubRepoDetailContract.AppView> extends BaseRxPresenter<V>
        implements GitHubRepoDetailContract.Presenter<V>{

    @Inject
    GitHubApiService gitHubApiService;
    @Inject
    SchedulerProviderContract schedulerProviderContract;

    @Inject
    public GitHubRepoDetailPresenter(GitHubApiService gitHubApiService,
                               SchedulerProviderContract schedulerProviderContract,
                               CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
        this.schedulerProviderContract = schedulerProviderContract;
        this.gitHubApiService = gitHubApiService;
    }

    @Override
    public void onPullRequestCellClick(PullRequestModel pullRequestModel) {
        getView().openPullRequestDetailOnBrowser(pullRequestModel.getHtmlUrl());
    }

    @Override
    public void getPullRequestListAtPage(String creatorOfRepository, String repositoryName, int page) {
        getCompositeDisposable().add(
            gitHubApiService.getPullRequestList(creatorOfRepository, repositoryName, page)
            .subscribeOn(schedulerProviderContract.io())
            .observeOn(schedulerProviderContract.ui())
            .doOnSubscribe(__ -> getView().showLoading())
            .doAfterTerminate(() -> getView().hideLoading())
            .subscribeWith(new DisposableSingleObserver<List<PullRequestModel>>() {
                @Override
                public void onSuccess(List<PullRequestModel> pullRequestModelList) {
                    getView().onPullRequestListReceived(pullRequestModelList);
                }

                @Override
                public void onError(Throwable e) {
                    getView().showMessage(e.getLocalizedMessage());
                }
            })
        );
    }
}
