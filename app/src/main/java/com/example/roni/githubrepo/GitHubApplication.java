package com.example.roni.githubrepo;

import android.app.Application;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by roni on 18/02/18.
 */

public class GitHubApplication extends Application {
    private static GitHubApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static Application getInstance(){
        return sInstance;
    }

    /**
     * Format a date string into another one with the format MMM dd, yyyy
     *
     * @param inputDateString the date string to be formatted
     * @return a string formatted according to the mask
     */
    public static String formatStringDateToMonthDayAndYear(String inputDateString) {
        String inputDateMaskFormat = "yyyy-MM-dd'T'HH:mm:SS'Z'"; // has the format of the input date
        String outputDateMaskFormat = "MMM dd, yyyy"; // the mask format of the output to be shown on view
        Date parsed;
        String outputDate;

        SimpleDateFormat df_input = new SimpleDateFormat(inputDateMaskFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputDateMaskFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDateString);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            outputDate = inputDateString;
        }
        return outputDate;
    }
}
