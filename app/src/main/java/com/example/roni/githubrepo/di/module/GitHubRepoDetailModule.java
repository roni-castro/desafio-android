package com.example.roni.githubrepo.di.module;

import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailContract;
import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by roni on 20/02/18.
 */

@Module
public class GitHubRepoDetailModule {

    @Provides
    GitHubRepoDetailContract.Presenter<GitHubRepoDetailContract.AppView> provideGitHubRepoDetailPresenter(
            GitHubRepoDetailPresenter<GitHubRepoDetailContract.AppView> presenter) {
        return presenter;
    }
}
