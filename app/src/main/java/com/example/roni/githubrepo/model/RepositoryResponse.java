package com.example.roni.githubrepo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by roni on 19/02/18.
 */

public class RepositoryResponse {
    @SerializedName("total_count")          private long totalCount;
    @SerializedName("incomplete_results")   private boolean incompleteResults;
    @SerializedName("items")                private List<RepositoryModel> repositoryModelList;

    public long getTotalCount() {
        return totalCount;
    }

    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    public List<RepositoryModel> getRepositoryList() {
        return repositoryModelList;
    }
}
