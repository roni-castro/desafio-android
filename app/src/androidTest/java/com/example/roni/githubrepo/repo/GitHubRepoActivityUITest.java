package com.example.roni.githubrepo.repo;


import android.support.annotation.IdRes;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.example.roni.githubrepo.R;
import com.example.roni.githubrepo.ui.repo.GitHubRepoActivity;
import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class GitHubRepoActivityUITest {
    private ViewInteraction repoRecyclerViewInteraction;

    @Rule
    public ActivityTestRule<GitHubRepoActivity> mActivityTestRule = new ActivityTestRule<>(GitHubRepoActivity.class);

    @Before
    public void setUpTest(){
        repoRecyclerViewInteraction = onView(
                allOf(withId(R.id.recycler_view_github_repo), isDisplayed(),
                        childAtPosition(
                                withClassName(is("android.support.constraint.ConstraintLayout")),
                                0)));
        Intents.init();
    }

    @After
    public void tearDown(){
        Intents.release();
    }

    @Test
    public void testRecyclerViewShouldScrollToTheLastElementTest() {
        int totalItemOnRecyclerView = getCountFromRecyclerView(R.id.recycler_view_github_repo);
        repoRecyclerViewInteraction.perform(scrollToPosition(totalItemOnRecyclerView -1));
    }

    @Test
    public void testRecyclerViewClickOnFistItemAndGoToDetailScreen() {
        repoRecyclerViewInteraction.perform(scrollToPosition(0), click());
        intended(hasComponent(GitHubRepoDetailActivity.class.getName()));
    }

    @Test
    public void testFlowFromRepositoryListToPullRequestWithSuccess() {
        // Click on the first repository cell
        repoRecyclerViewInteraction.perform(actionOnItemAtPosition(0, click()));

        // Get Pull Request recycler view
        ViewInteraction pullRequestRecyclerView = onView(
                allOf(withId(R.id.recycler_view_github_pull_request),
                        childAtPosition(
                                withClassName(is("android.support.constraint.ConstraintLayout")),
                                0)));
        // Click on the first pull request cell
        pullRequestRecyclerView.perform(actionOnItemAtPosition(0, click()));
    }

    private static int getCountFromRecyclerView(@IdRes int RecyclerViewId) {
        final int[] COUNT = {0};
        Matcher matcher = new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                COUNT[0] = ((RecyclerView) item).getAdapter().getItemCount();
                return true;
            }
            @Override
            public void describeTo(Description description) {}
        };
        onView(allOf(withId(RecyclerViewId),isDisplayed())).check(matches(matcher));
        return COUNT[0];
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
