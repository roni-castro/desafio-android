package com.example.roni.githubrepo.ui.repo_detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.roni.githubrepo.R;
import com.example.roni.githubrepo.di.component.DaggerGitHubRepoDetailComponent;
import com.example.roni.githubrepo.di.component.GitHubRepoDetailComponent;
import com.example.roni.githubrepo.di.module.AppModule;
import com.example.roni.githubrepo.di.module.ContextModule;
import com.example.roni.githubrepo.model.PullRequestModel;
import com.example.roni.githubrepo.ui.base.BaseFragment;
import com.example.roni.githubrepo.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class GitHubRepoDetailFragment extends BaseFragment
        implements GitHubRepoDetailContract.AppView, GitHubPullRequestAdapterCellClickListener {

    @BindView(R.id.recycler_view_github_pull_request)
    RecyclerView mRecyclerView;
    GitHubPullRequestDetailAdapter mAdapter;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    // Variables used to save the state of the pull request list during rotation
    private static final String LAYOUT_MANAGER_POSITION_STATE_KEY = "LAYOUT_MANAGER_POSITION_STATE_KEY";
    private static final String LIST_STATE_KEY = "LIST_STATE_KEY";
    private static final String CURRENT_PAGE_STATE_KEY = "CURRENT_PAGE_STATE_KEY";

    private static final String OWNER_LOGIN_FRAGMENT_EXTRA = "OWNER_LOGIN_FRAGMENT_EXTRA";
    private static final String REPOSITORY_NAME_FRAGMENT_EXTRA = "REPOSITORY_NAME_FRAGMENT_EXTRA";
    private String repositoryName;
    private String creatorOfRepository;

    @Inject
    GitHubRepoDetailContract.Presenter<GitHubRepoDetailContract.AppView> presenter;

    public GitHubRepoDetailFragment() {
    }

    @Override
    protected int getFragmentViewResId() {
        return R.layout.fragment_git_hub_repo_detail;
    }

    @Override
    protected void setupInjectionComponent() {
        GitHubRepoDetailComponent appComponent = DaggerGitHubRepoDetailComponent.builder()
                .contextModule(new ContextModule(getActivity()))
                .appModule(new AppModule())
                .build();
        appComponent.inject(this);
    }

    @Override
    public void attachViewToPresenter() {
        presenter.onAttachView(this);
    }

    @Override
    public void detachViewToPresenter() {
        presenter.onDetachView();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        if(savedInstanceState == null){
            setUpRecyclerViewWithNoData();
            presenter.getPullRequestListAtPage(
                    creatorOfRepository,
                    repositoryName,
                    endlessRecyclerOnScrollListener.getCurrentPage());
        } else{ // Recover list state
            setUpRecyclerViewWithExistingData(savedInstanceState);
        }
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            repositoryName = (getArguments().getString(REPOSITORY_NAME_FRAGMENT_EXTRA));
            creatorOfRepository = (getArguments().getString(OWNER_LOGIN_FRAGMENT_EXTRA));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save list state
        outState.putParcelableArrayList(LIST_STATE_KEY, mAdapter.getItems());
        outState.putInt(LAYOUT_MANAGER_POSITION_STATE_KEY, mRecyclerView.getVerticalScrollbarPosition());
        outState.putInt(CURRENT_PAGE_STATE_KEY, endlessRecyclerOnScrollListener.getCurrentPage());
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of this fragment .
     */
    public static GitHubRepoDetailFragment newInstance(String repositoryName, String ownerName) {
        GitHubRepoDetailFragment fragment = new GitHubRepoDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(REPOSITORY_NAME_FRAGMENT_EXTRA, repositoryName);
        bundle.putString(OWNER_LOGIN_FRAGMENT_EXTRA, ownerName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void openPullRequestDetailOnBrowser(String pullRequestUrl) {
        if (pullRequestUrl != null && pullRequestUrl.length() > 0) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequestUrl));
            startActivity(browserIntent);
        } else{
           showLoading();
        }
    }

    @Override
    public void onPullRequestListReceived(List<PullRequestModel> pullRequestModelList) {
        mAdapter.addAllItemsAtEndOfList(pullRequestModelList);
    }

    // HELPER METHODS
    private void setUpRecyclerView(GitHubPullRequestDetailAdapter adapter,
                                   int currentPage){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(adapter);
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                presenter.getPullRequestListAtPage(
                        creatorOfRepository,
                        repositoryName,
                        page);
            }
        };
        endlessRecyclerOnScrollListener.setCurrentPage(currentPage);
        mRecyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);
    }

    private void setUpRecyclerViewWithNoData(){
        mAdapter = new GitHubPullRequestDetailAdapter(getActivity(),this, new ArrayList<>());
        setUpRecyclerView(mAdapter, 1);
    }

    private void setUpRecyclerViewWithExistingData(Bundle savedInstanceState){
        ArrayList<PullRequestModel> pullRequestModels = savedInstanceState.getParcelableArrayList(LIST_STATE_KEY);
        int verticalScrollPosition = savedInstanceState.getInt(LAYOUT_MANAGER_POSITION_STATE_KEY);
        int currentPageState = savedInstanceState.getInt(CURRENT_PAGE_STATE_KEY);

        mRecyclerView.setVerticalScrollbarPosition(verticalScrollPosition);
        mAdapter = new GitHubPullRequestDetailAdapter(getActivity(),this, pullRequestModels);
        setUpRecyclerView(mAdapter, currentPageState);
    }

    @Override
    public void onPullRequestCellClick(PullRequestModel pullRequestModel) {
        presenter.onPullRequestCellClick(pullRequestModel);
    }
}
