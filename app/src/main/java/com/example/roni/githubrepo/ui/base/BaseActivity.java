package com.example.roni.githubrepo.ui.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.roni.githubrepo.GitHubApplication;
import com.example.roni.githubrepo.utils.ProgressBarHandler;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by roni on 17/02/18.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView {
    private Unbinder mUnbinder;
    private ProgressBarHandler mProgressBarHandler;

    // Method to be implemented by all child activities
    protected abstract @LayoutRes int getContentView();

    // Lifecycle methods
    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        mUnbinder = ButterKnife.bind(this);
    }

    @CallSuper
    @Override
    protected void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // BASE VIEW METHODS
    @Override
    public void showMessage(int stringResId) {
        showMessage(getString(stringResId));
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(GitHubApplication.getInstance().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoading() {
        if(mProgressBarHandler == null) {
            mProgressBarHandler = new ProgressBarHandler(this);
        }
        mProgressBarHandler.show();
    }

    @Override
    public void hideLoading() {
        mProgressBarHandler.hide();
    }

    // HELPER METHODS
    protected void setFragment(int containerResId, Fragment fragment, String fragmentTag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerResId, fragment, fragmentTag);
        fragmentTransaction.commit();
    }
}
