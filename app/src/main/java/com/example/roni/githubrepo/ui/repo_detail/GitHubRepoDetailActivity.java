package com.example.roni.githubrepo.ui.repo_detail;

import android.content.Intent;
import android.os.Bundle;

import com.example.roni.githubrepo.R;
import com.example.roni.githubrepo.ui.base.BaseActivity;
import com.example.roni.githubrepo.ui.repo.GitHubRepoActivity;

public class GitHubRepoDetailActivity extends BaseActivity {
    private static final String GITHUB_REPO_DETAIL_FRAGMENT = "GITHUB_REPO_DETAIL_FRAGMENT";

    @Override
    protected int getContentView() {
        return R.layout.activity_git_hub_repo_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String repositoryName = intent.getStringExtra(GitHubRepoActivity.REPOSITORY_NAME_EXTRA);
        String repositoryOwnerLogin = intent.getStringExtra(GitHubRepoActivity.OWNER_LOGIN_EXTRA);
        setUpToolbar(repositoryName);

        // Set up the fragment of this activity
        GitHubRepoDetailFragment fragment = (GitHubRepoDetailFragment)
                getSupportFragmentManager().findFragmentByTag(GITHUB_REPO_DETAIL_FRAGMENT);
        if(fragment == null){
            fragment = GitHubRepoDetailFragment.newInstance(repositoryName, repositoryOwnerLogin);
        }
        setFragment(R.id.fragment_holder, fragment, GITHUB_REPO_DETAIL_FRAGMENT);
    }

    private void setUpToolbar(String title){
        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
