package com.example.roni.githubrepo.ui.repo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.roni.githubrepo.R;
import com.example.roni.githubrepo.model.RepositoryModel;
import com.example.roni.githubrepo.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by roni on 18/02/18.
 */

public class GitHubRepoAdapter extends RecyclerView.Adapter<GitHubRepoAdapter.GitHubRepoViewHolder> {
    private ArrayList<RepositoryModel> repositoryModelList;
    private Context context;
    private GitHubRepoAdapterCellClickListener listener;
    private Picasso mPicasso;

    public GitHubRepoAdapter(Context context,
                             GitHubRepoAdapterCellClickListener listener,
                             ArrayList<RepositoryModel> repositoryModelList) {
        this.context = context;
        this.listener = listener;
        this.repositoryModelList = repositoryModelList;
        mPicasso = Picasso.with(context);
    }

    public ArrayList<RepositoryModel> getItems(){
        return repositoryModelList;
    }

    public void addAllItemsAtEndOfList(List<RepositoryModel> list) {
        repositoryModelList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public GitHubRepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GitHubRepoViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_repo_view, parent, false));
    }

    @Override
    public void onBindViewHolder(GitHubRepoViewHolder holder, int position) {
        RepositoryModel repositoryModel = repositoryModelList.get(position);
        GitHubRepoViewHolder viewHolder = (GitHubRepoViewHolder) holder;
        // Set up repo data to view
        viewHolder.repoName.setText(repositoryModel.getName());
        viewHolder.repoDescription.setText(repositoryModel.getDescription());
        viewHolder.repoNumberOfForks.setText(String.valueOf(repositoryModel.getNumberOfForks()));
        viewHolder.repoNumberOfStars.setText(String.valueOf(repositoryModel.getNumberOfStars()));

        // Set up user data to view
        UserModel userModel = repositoryModel.getUserModel();
        viewHolder.repoUserName.setText(userModel.getUserName());

        //Set up the avatar image
        if (userModel.getAvatarUrl() != null && userModel.getAvatarUrl().length() > 0) {
            mPicasso.load(userModel.getAvatarUrl())
                    .noFade()
                    .placeholder(R.drawable.ic_person_gray_36px)
                    .into(viewHolder.repoUserAvatar);
        } else {
            viewHolder.repoUserAvatar.setImageDrawable(
                    context.getResources().getDrawable(R.drawable.ic_person_gray_36px));
        }
    }

    @Override
    public int getItemCount() {
        return repositoryModelList.size();
    }

    public class GitHubRepoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_repo_name)
        TextView repoName;
        @BindView(R.id.txt_repo_description)
        TextView repoDescription;
        @BindView(R.id.txt_repo_number_of_forks)
        TextView repoNumberOfForks;
        @BindView(R.id.txt_repo_number_of_stars)
        TextView repoNumberOfStars;
        @BindView(R.id.txt_user_name)
        TextView repoUserName;
        @BindView(R.id.img_user_avatar)
        CircleImageView repoUserAvatar;
        @BindView(R.id.cell_repo_layout)
        ViewGroup cellLayout;

        public GitHubRepoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // CLick event on the cell
            cellLayout.setOnClickListener(view -> {
                if(getAdapterPosition() >= 0) {
                    listener.onRepositoryCellClick(repositoryModelList.get(getAdapterPosition()));
                }
            });
        }
    }
}

interface GitHubRepoAdapterCellClickListener{
    void onRepositoryCellClick(RepositoryModel repositoryModel);
}
