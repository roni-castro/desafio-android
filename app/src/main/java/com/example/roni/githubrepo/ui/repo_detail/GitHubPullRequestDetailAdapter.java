package com.example.roni.githubrepo.ui.repo_detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.roni.githubrepo.GitHubApplication;
import com.example.roni.githubrepo.R;
import com.example.roni.githubrepo.model.PullRequestModel;
import com.example.roni.githubrepo.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by roni on 20/02/18.
 */

public class GitHubPullRequestDetailAdapter extends RecyclerView.Adapter<GitHubPullRequestDetailAdapter.GitHubPullRequestDetailViewHolder> {
    private ArrayList<PullRequestModel> pullRequestModelList;
    private Context context;
    private GitHubPullRequestAdapterCellClickListener listener;
    private Picasso mPicasso;

    public GitHubPullRequestDetailAdapter(Context context,
                             GitHubPullRequestAdapterCellClickListener listener,
                             ArrayList<PullRequestModel> pullRequestModelList) {
        this.context = context;
        this.listener = listener;
        this.pullRequestModelList = pullRequestModelList;
        mPicasso = Picasso.with(context);
    }

    public ArrayList<PullRequestModel> getItems(){
        return pullRequestModelList;
    }

    public void addAllItemsAtEndOfList(List<PullRequestModel> list) {
        pullRequestModelList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public GitHubPullRequestDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GitHubPullRequestDetailViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_pull_request_view, parent, false));
    }

    @Override
    public void onBindViewHolder(GitHubPullRequestDetailViewHolder holder, int position) {
        PullRequestModel pullRequestModel = pullRequestModelList.get(position);
        GitHubPullRequestDetailViewHolder viewHolder = (GitHubPullRequestDetailViewHolder) holder;
        // Set up the pull request data to view
        viewHolder.pullRequestTitle.setText(pullRequestModel.getTitle());
        viewHolder.pullRequestBody.setText(pullRequestModel.getBody());
        viewHolder.pullRequestCreatedAt.setText(
                context.getString(R.string.created_pull_request_date,
                GitHubApplication.formatStringDateToMonthDayAndYear(pullRequestModel.getCreatedAt()))
        );

        // Set up user data to view
        UserModel userModel = pullRequestModel.getUser();
        viewHolder.pullRequestUserName.setText(
                context.getString(R.string.created_pull_request_author, userModel.getUserName()));

        //Set up the avatar image
        if (userModel.getAvatarUrl() != null && userModel.getAvatarUrl().length() > 0) {
            mPicasso.load(userModel.getAvatarUrl())
                    .placeholder(R.drawable.ic_person_gray_36px)
                    .into(viewHolder.pullRequestUserAvatar);
        } else {
            viewHolder.pullRequestUserAvatar.setImageDrawable(
                    context.getResources().getDrawable(R.drawable.ic_person_gray_36px));
        }
    }

    @Override
    public int getItemCount() {
        return pullRequestModelList.size();
    }

     class GitHubPullRequestDetailViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_pull_request_title)
        TextView pullRequestTitle;
        @BindView(R.id.txt_pull_request_created_data)
        TextView pullRequestCreatedAt;
        @BindView(R.id.txt_pull_request_body)
        TextView pullRequestBody;
        @BindView(R.id.txt_user_name)
        TextView pullRequestUserName;
        @BindView(R.id.img_user_avatar)
        CircleImageView pullRequestUserAvatar;
        @BindView(R.id.cell_pull_request_layout)
        ViewGroup cellLayout;

        public GitHubPullRequestDetailViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // CLick event on the cell
            cellLayout.setOnClickListener(view -> {
                if(getAdapterPosition() >= 0) {
                    listener.onPullRequestCellClick(pullRequestModelList.get(
                            GitHubPullRequestDetailViewHolder.this.getAdapterPosition()));
                }
            });
        }
    }
}

interface GitHubPullRequestAdapterCellClickListener {
    void onPullRequestCellClick(PullRequestModel pullRequestModel);
}
