package com.example.roni.githubrepo.di.module;

import com.example.roni.githubrepo.data.GitHubApiService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by roni on 18/02/18.
 */

@Module(includes = OkHttpClientModule.class)
public class NetworkModule {
    private static final String BASE_URL = "https://api.github.com/";

    @Provides
    public GitHubApiService getGitHubApiService(Retrofit retrofit){
        return retrofit.create(GitHubApiService.class);
    }

    @Provides
    public Retrofit retrofit(OkHttpClient okHttpClient,
                             RxJava2CallAdapterFactory rxJavaCallAdapterFactory,
                             GsonConverterFactory gsonConverterFactory){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    public GsonConverterFactory gsonConverterFactory(Gson gson){
        return GsonConverterFactory.create(gson);
    }

    @Provides
    public RxJava2CallAdapterFactory rxJavaCallAdapterFactory(){
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    public Gson gson(){
        return new GsonBuilder().create();
    }
}
