package com.example.roni.githubrepo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by roni on 20/02/18.
 */

public class PullRequestModel  implements Parcelable {
    @SerializedName("user")
    @Expose
    private UserModel user;
    @Expose
    @SerializedName("created_at")
    private String createdAt;
    @Expose
    private long id;
    @Expose
    private String title;
    @Expose
    @SerializedName("html_url")
    private String htmlUrl;
    @Expose
    private String body;

    public PullRequestModel(UserModel user, String createdAt, long id, String title, String htmlUrl, String body) {
        this.user = user;
        this.createdAt = createdAt;
        this.id = id;
        this.title = title;
        this.htmlUrl = htmlUrl;
        this.body = body;
    }

    protected PullRequestModel(Parcel in) {
        createdAt = in.readString();
        id = in.readLong();
        title = in.readString();
        htmlUrl = in.readString();
        body = in.readString();
    }

    public static final Creator<PullRequestModel> CREATOR = new Creator<PullRequestModel>() {
        @Override
        public PullRequestModel createFromParcel(Parcel in) {
            return new PullRequestModel(in);
        }

        @Override
        public PullRequestModel[] newArray(int size) {
            return new PullRequestModel[size];
        }
    };

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(createdAt);
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(htmlUrl);
        parcel.writeString(body);
    }
}
