package com.example.roni.githubrepo;

import com.example.roni.githubrepo.data.SchedulerProviderContract;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by roni on 18/02/18.
 */


public class TestSchedulerProvider implements SchedulerProviderContract {

    @Override
    public Scheduler computation() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler ui() {
        return Schedulers.trampoline();
    }
}
