package com.example.roni.githubrepo.repo_detail;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.IdRes;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.example.roni.githubrepo.R;
import com.example.roni.githubrepo.ui.repo.GitHubRepoActivity;
import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class GitHubRepoDetailActivityTest {
    private  ViewInteraction pullRequestRecyclerView;
    private static final String REPOSITORY_NAME_EXTRA = "java-design-patterns";
    private static final String OWNER_LOGIN_EXTRA = "iluwatar";

    @Rule
    public ActivityTestRule<GitHubRepoDetailActivity> mActivityTestRule2 = new ActivityTestRule<>(GitHubRepoDetailActivity.class, true, false);

    @Before
    public void setUpTest() {
        // Get Pull Request recycler view
        pullRequestRecyclerView = onView(
                allOf(withId(R.id.recycler_view_github_pull_request),
                childAtPosition(
                        withClassName(is("android.support.constraint.ConstraintLayout")),
                        0)));
        Intents.init();
    }

    @After
    public void tearDown(){
        Intents.release();
    }

    @Test
    public void testRecyclerViewShouldScrollToTheLastElement() {
        startPullRequestActivityWithIntent(REPOSITORY_NAME_EXTRA, OWNER_LOGIN_EXTRA);

        int totalItemOnRecyclerView = getCountFromRecyclerView(R.id.recycler_view_github_pull_request);
        pullRequestRecyclerView.perform(scrollToPosition(totalItemOnRecyclerView -1));
    }

    @Test
    public void testIfRecyclerViewIsShownAndHasToolbarWithTheRepoNameTitle() {
        startPullRequestActivityWithIntent(REPOSITORY_NAME_EXTRA, OWNER_LOGIN_EXTRA);

        onView(withId(R.id.recycler_view_github_pull_request)).check(matches(isDisplayed()));
        onView(withText(REPOSITORY_NAME_EXTRA)).check(matches(isDisplayed()));
    }

    @Test
    public void testIfRotationWillNotCrash() {
        startPullRequestActivityWithIntent(REPOSITORY_NAME_EXTRA, OWNER_LOGIN_EXTRA);

        mActivityTestRule2.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mActivityTestRule2.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mActivityTestRule2.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void startPullRequestActivityWithIntent(String repositoryName, String creatorUserName){
        Intent repositoryIntent = new Intent();
        repositoryIntent.putExtra(GitHubRepoActivity.REPOSITORY_NAME_EXTRA, repositoryName);
        repositoryIntent.putExtra(GitHubRepoActivity.OWNER_LOGIN_EXTRA, creatorUserName);

        mActivityTestRule2.launchActivity(repositoryIntent);
    }

    private static int getCountFromRecyclerView(@IdRes int RecyclerViewId) {
        final int[] COUNT = {0};
        Matcher matcher = new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                COUNT[0] = ((RecyclerView) item).getAdapter().getItemCount();
                return true;
            }
            @Override
            public void describeTo(Description description) {}
        };
        onView(allOf(withId(RecyclerViewId),isDisplayed())).check(matches(matcher));
        return COUNT[0];
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
