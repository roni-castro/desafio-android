package com.example.roni.githubrepo;

import com.example.roni.githubrepo.data.GitHubApiService;
import com.example.roni.githubrepo.model.PullRequestModel;
import com.example.roni.githubrepo.model.RepositoryModel;
import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailContract;
import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailFragment;
import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GitHubRepoDetailUnitTest {
    private GitHubRepoDetailPresenter<GitHubRepoDetailContract.AppView> presenter;
    private static final String REPOSITORY_NAME = "bitcoin";
    private static final String CREATOR_OF_REPOSITORY = "bitcoin";
    private static final String VALID_URL = "https://api.github.com/repos/bitcoin/bitcoin/pulls/12495";
    private static final String INVALID_URL = "";
    private static final int PAGE = 1;

    @Mock
    private GitHubApiService gitHubApiService;

    @Mock
    private GitHubRepoDetailFragment appView;

    @Before
    public void setUpTest() {
        MockitoAnnotations.initMocks(this);

        presenter = new GitHubRepoDetailPresenter<>(
                gitHubApiService,
                new TestSchedulerProvider(),
                new CompositeDisposable());
        presenter.onAttachView(appView);
    }

    @After
    public void tearDown() throws Exception {
        presenter.onDetachView();
    }

    @Test
    public void testIfPresenterTellViewToOpenWebBrowserWithAValidUrl() {
        PullRequestModel pullRequestModel = mock(PullRequestModel.class);

        when(pullRequestModel.getHtmlUrl()).thenReturn(VALID_URL);
        presenter.onPullRequestCellClick(pullRequestModel);
        verify(appView).openPullRequestDetailOnBrowser(VALID_URL);
    }

    @Test
    public void testIfSuccessRequestWillReturnListOfItemsToView() {
        PullRequestModel pullRequestModel = mock(PullRequestModel.class);
        List<PullRequestModel> pullRequestModelList = new ArrayList<>();
        pullRequestModelList.add(pullRequestModel);

        when(gitHubApiService.getPullRequestList(any(String.class),any(String.class), any(Integer.class) ))
                .thenReturn(Single.just(pullRequestModelList));

        presenter.getPullRequestListAtPage(CREATOR_OF_REPOSITORY, REPOSITORY_NAME, PAGE);
        verify(appView).showLoading();
        verify(appView).hideLoading();
        verify(appView).onPullRequestListReceived(pullRequestModelList);
    }

    @Test
    public void testIfRequestFailWillShowMessage() {

        when(gitHubApiService.getPullRequestList(any(String.class), any(String.class), anyInt()))
                .thenReturn(Single.error(new Exception("Error")));

        presenter.getPullRequestListAtPage(CREATOR_OF_REPOSITORY, REPOSITORY_NAME, PAGE);
        verify(appView).showLoading();
        verify(appView).hideLoading();
        verify(appView).showMessage("Error");
    }
}