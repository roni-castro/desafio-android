package com.example.roni.githubrepo.di.module;

import android.support.v4.app.FragmentActivity;

import com.example.roni.githubrepo.data.SchedulerProvider;
import com.example.roni.githubrepo.data.SchedulerProviderContract;
import com.example.roni.githubrepo.ui.repo.GitHubRepoContract;
import com.example.roni.githubrepo.ui.repo.GitHubRepoPresenter;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by roni on 18/02/18.
 */
@Module
public class AppModule {

    @Provides
    SchedulerProviderContract schedulerProviderContract(){
        return new SchedulerProvider();
    }


    @Provides
    CompositeDisposable compositeDisposable(){
        return new CompositeDisposable();
    }
}
