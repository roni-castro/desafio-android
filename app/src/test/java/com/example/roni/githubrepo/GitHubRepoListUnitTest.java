package com.example.roni.githubrepo;

import com.example.roni.githubrepo.data.GitHubApiService;
import com.example.roni.githubrepo.model.RepositoryModel;
import com.example.roni.githubrepo.model.RepositoryResponse;
import com.example.roni.githubrepo.ui.repo.GitHubRepoContract;
import com.example.roni.githubrepo.ui.repo.GitHubRepoFragment;
import com.example.roni.githubrepo.ui.repo.GitHubRepoPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GitHubRepoListUnitTest {

    private GitHubRepoPresenter<GitHubRepoContract.AppView> presenter;

    @Mock
    private GitHubApiService gitHubApiService;

    @Mock
    private GitHubRepoFragment appView;

    @Before
    public void setUpTest() {
        MockitoAnnotations.initMocks(this);

        presenter = new GitHubRepoPresenter<>(
                gitHubApiService,
                new TestSchedulerProvider(),
                new CompositeDisposable());
        presenter.onAttachView(appView);
    }

    @After
    public void tearDown() throws Exception {
        presenter.onDetachView();
    }

    @Test
    public void testIfPresenterTellViewToGoToGitHubDetailScreen() {
        RepositoryModel repositoryModel = mock(RepositoryModel.class);
        presenter.onRepoCellClick(repositoryModel);
        verify(appView).goToGitHubDetailActivity(repositoryModel);
    }

    @Test
    public void testIfSuccessRequestWillReturnListOfItemsToView() {
        RepositoryModel repositoryModel = mock(RepositoryModel.class);
        List<RepositoryModel> repositoryModelList = new ArrayList<>();
        repositoryModelList.add(repositoryModel);

        RepositoryResponse repositoryResponse = mock(RepositoryResponse.class);
        when(repositoryResponse.getRepositoryList()).thenReturn(repositoryModelList);

        when(gitHubApiService.getRepositoryList(any(String.class),any(String.class), any(Integer.class) ))
                .thenReturn(Single.just(repositoryResponse));

        int page = 1;
        presenter.getGitHubRepoAtPage(page);
        verify(appView).showLoading();
        verify(appView).hideLoading();
        verify(appView).addReposItemToList(repositoryModelList);
    }

    @Test
    public void testIfRequestFailWillShowMessage() {

        when(gitHubApiService.getRepositoryList(any(String.class), any(String.class), anyInt()))
                .thenReturn(Single.error(new Exception("Error")));

        int page = 1;
        presenter.getGitHubRepoAtPage(page);
        verify(appView).showLoading();
        verify(appView).hideLoading();
        verify(appView).showMessage("Error");
    }
}