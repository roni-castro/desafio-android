package com.example.roni.githubrepo.ui.repo;

import com.example.roni.githubrepo.model.RepositoryModel;
import com.example.roni.githubrepo.ui.base.BasePresenterContract;
import com.example.roni.githubrepo.ui.base.BaseView;

import java.util.List;

public interface GitHubRepoContract{

    interface AppView extends BaseView {
        void goToGitHubDetailActivity(RepositoryModel repositoryModel);
        void addReposItemToList(List<RepositoryModel> repositoryModelList);
    }

    interface Presenter<V extends GitHubRepoContract.AppView> extends BasePresenterContract<V>{
        void onRepoCellClick(RepositoryModel repositoryModel);
        void getGitHubRepoAtPage(final int page);
    }

}
