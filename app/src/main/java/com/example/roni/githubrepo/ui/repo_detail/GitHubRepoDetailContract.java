package com.example.roni.githubrepo.ui.repo_detail;

import com.example.roni.githubrepo.model.PullRequestModel;
import com.example.roni.githubrepo.ui.base.BasePresenterContract;
import com.example.roni.githubrepo.ui.base.BaseView;

import java.util.List;

/**
 * Created by roni on 20/02/18.
 */

public interface GitHubRepoDetailContract {

    interface AppView extends BaseView {
        void openPullRequestDetailOnBrowser(String pullRequestUrl);

        void onPullRequestListReceived(List<PullRequestModel> pullRequestModelList);
    }

    interface Presenter<V extends GitHubRepoDetailContract.AppView> extends BasePresenterContract<V> {

        void onPullRequestCellClick(PullRequestModel pullRequestModel);

        void getPullRequestListAtPage(String creatorOfRepository, String repositoryName, int page);
    }
}
