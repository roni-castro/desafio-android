package com.example.roni.githubrepo.ui.base;

/**
 * Created by roni on 18/02/18.
 */

public interface BasePresenterContract<V extends BaseView> {
    // Update the view instance inside the presenter
    void onAttachView(V view);

    // View was unsubscribed to the presenter
    void onDetachView();
}
