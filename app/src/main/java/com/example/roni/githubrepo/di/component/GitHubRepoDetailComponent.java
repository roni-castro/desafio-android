package com.example.roni.githubrepo.di.component;

import com.example.roni.githubrepo.di.module.AppModule;
import com.example.roni.githubrepo.di.module.GitHubRepoDetailModule;
import com.example.roni.githubrepo.di.module.NetworkModule;
import com.example.roni.githubrepo.ui.repo_detail.GitHubRepoDetailFragment;

import dagger.Component;

/**
 * Created by roni on 20/02/18.
 */

@Component(modules = {NetworkModule.class, AppModule.class, GitHubRepoDetailModule.class})
public interface GitHubRepoDetailComponent {

    void inject(GitHubRepoDetailFragment fragment);
}
