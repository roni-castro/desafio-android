package com.example.roni.githubrepo.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by roni on 17/02/18.
 */

public interface BaseView {

    void showMessage(@StringRes int stringId);

    void showMessage(String message);

    void showLoading();

    void hideLoading();
}
